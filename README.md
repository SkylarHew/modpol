# Modpol for Minetest

![](lib/empire-modpol.png)

Modpol, short for "modular politics," enables diverse governance processes on multi-user platforms. It offers a library with which users can choose, modify, and create modules that add specific governance functionalities.

This implementation is a mod for [Minetest](https://minetest.net), a free/open-source voxel game. It is designed to be adapted to other multi-user platforms that also employ Lua as an extension language.

**Learn more at [modpol.net](https://modpol.net).**

## Installation in Minetest

To use this in Minetest, simply install it in your `mods/` or `worldmods/` folder. Minetest will load `init.lua`.

In the game, open the Modpol dashboard with the command `/mp`.

For testing purposes, players with the `privs` privilege (generally admins) can use the `/mptest` command, which resets all the orgs and opens a dashboard.\


## Standalone Version on the Command Line

Modpol can also be used independently of Minetest as a command-line tool. Currently command-line use of modpol requires a Unix-style system, but it is intended to become more fully platform independent.

The command-line version is in the `modpol` subdirectory. To run the program on Unix systems in CLI mode, install lua or luajit and execute the following in this directory:

```
$ lua[jit] login.lua
```

Alternatively, to test arbitrary functions in the interpreter outside of the interactive dashboards, load Modpol's library with:

```
$ lua[jit]
> dofile("modpol_core/modpol.lua")
```

In the interpreter, for a list of global functions and tables, use `modpol.menu()`.

## Storage

The persistent storage method may be chosen in `modpol.lua`. If available, Modpol uses Minetest's built-in StorageRef system for Minetest 5.*. If that is not available, or in CLI mode, data will be stored in a data directory at `modpol_core/data/`. This will contain a log file and serialized program data files.


## Documentation

Various guides are available at the [GitLab wiki](https://gitlab.com/medlabboulder/modpol/-/wikis/home).

Read documentation of functions and modules at `docs/doc/index.html`. Documentation was generated using [LDoc](https://stevedonovan.github.io/ldoc/). To generate basic documentation for every page, download `ldoc` and use the following command:

```
$ cd docs/
$ ldoc ..
```

This will not generate the same index page and sidebar as the documentation provided; the appropriate structure needs to be added manually.

## Credits

This project is led by [Nathan Schneider](https://nathanschneider.info) of the [Media Enterprise Design Lab](https://colorado.edu/lab/medlab) at the University of Colorado Boulder, as part of the [Metagovernance Project](https://metagov.org).

Contributors include:

* [Luke Miller](https://gitlab.com/lukvmil) (co-leadership, main control flow, object orientation, module spec)
* [MisterE](https://gitlab.com/gbrrudmin) (early project refactoring, core feature development)
* Robert Kiraly [[OldCoder](https://github.com/oldcoder/)] (ocutils.lua, storage-local.lua, project refactoring)
* Skylar Hew (documentation)

We are grateful for initial support for this project from a residency with [The Bentway Conservancy](https://www.thebentway.ca/). Read about us in _[The Field Guide to Digital and/as Public Space](https://www.thebentway.ca/stories/field-guide/)_.


## Contributing

We'd love to welcome more contributors. Please join the conversation in the [Issues](https://gitlab.com/medlabboulder/modpol/-/issues), the \#modpol channel at the [Metagovernance Project](https://metagov.org) Slack, and the [Minetest.net forum](https://forum.minetest.net/viewtopic.php?f=47&t=26037).

Learn more about the project and how to develop your own modules in [the wiki](https://gitlab.com/medlabboulder/modpol/-/wikis/home).


## Licenses

* [Project](LICENSE.mt): MIT
* [Lua Serpent Serializer](serpent/LICENSE.txt): MIT
