--call all files in this directory

local localdir = modpol.topdir

--orgs
dofile (localdir .. "/orgs/base.lua")
dofile (localdir .. "/orgs/process.lua")
dofile (localdir .. "/orgs/users.lua")

--interactions
dofile (localdir .. "/interactions/interactions.lua")

--modules
--TODO make this automatic and directory-based
dofile (localdir .. "/modules/add_child_org.lua")
dofile (localdir .. "/modules/change_modules.lua")
dofile (localdir .. "/modules/change_policy.lua")
dofile (localdir .. "/modules/consent.lua")
dofile (localdir .. "/modules/create_token.lua")
dofile (localdir .. "/modules/defer.lua")
dofile (localdir .. "/modules/display_policies.lua")
dofile (localdir .. "/modules/display_processes.lua")
dofile (localdir .. "/modules/join_org.lua")
dofile (localdir .. "/modules/leave_org.lua")
dofile (localdir .. "/modules/message_org.lua")
dofile (localdir .. "/modules/randomizer.lua")
dofile (localdir .. "/modules/remove_child_org.lua")
dofile (localdir .. "/modules/remove_member.lua")
dofile (localdir .. "/modules/remove_org.lua")
dofile (localdir .. "/modules/remove_process.lua")
dofile (localdir .. "/modules/rename_org.lua")
dofile (localdir .. "/modules/send_token.lua")
dofile (localdir .. "/modules/tokenomics.lua")
