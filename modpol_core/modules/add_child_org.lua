--- Adds a child org
-- @module add_child_org

local add_child_org = {
    name = "Add child org",
    slug = "add_child_org",
    desc = "Create a child org in the current one"
}
add_child_org.data = {
   child_name = "",
   result = false
}
add_child_org.config = {
   approval_module = false
}

--- Initiate consent for new child org
-- @function add_child_org:initiate(result)
-- @param result Callback if this module is embedded in other modules
function add_child_org:initiate(result)
   modpol.interactions.text_query(
      self.initiator,"Child org name: ",
      function(input)
         if input == "" then
            modpol.interactions.message(
               self.initiator,
               "No name entered for child org")
            modpol.interactions.org_dashboard(
               self.initiator, self.org.name)
            self.org:delete_process(self.id)            
            return
         elseif modpol.orgs.get_org(input) then
            modpol.interactions.message(
               self.initiator,
               "Org name already in use")
            modpol.interactions.org_dashboard(
               self.initiator, self.org.name)
            self.org:delete_process(self.id)
            if result then result() end
            return
         end
         self.data.child_name = input
         modpol.interactions.message(
               self.initiator,
               "Proposed child org: " .. input)
         -- initiate consent process
         self.data.result = result
         self:call_module(
            self.config.approval_module,
            self.initiator, 
            {
               prompt = "Create child org " ..
                  self.data.child_name .. "?",
               votes_required = #self.org.members
            },
            function()
               self:create_child_org()
            end
         )
         modpol.interactions.org_dashboard(
            self.initiator, self.org.name)
      end
   )
end

--- Create a new child org
-- @function add_child_org:create_child_org
function add_child_org:create_child_org()
   self.org:add_org(self.data.child_name, self.initiator)
   modpol.interactions.message_org(
      self.initiator,
      self.org.name,
      "Created child org in " .. self.org.name .. ": "
      .. self.data.child_name)
   if self.data.result then self.data.result() end
   self.org:delete_process(self.id)
end
      
modpol.modules.add_child_org = add_child_org
