--- change_modules
-- @module change_modules
-- Depends on consent

local change_modules = {
    name = "Change modules (consent)",
    slug = "change_modules",
    desc = "Add or remove modules from the org with member consent",
    hide = false;
}

change_modules.data = {
   result = nil
}

change_modules.config = {
}

--- Change modules initiate
-- @function change_modules:initiate
-- @param result Callback if this module is embedded in other modules
function change_modules:initiate(result)
   self.data.result = result
   -- Step 1: add or remove?
   modpol.interactions.dropdown_query(
      self.initiator, "Module change options:",
      {"Add module","Remove module"},
      function(input)
         if input == "Add module" then
            self:add_module()
         elseif input == "Remove module" then
            self:remove_module()
         end
      end
   )
end

--- Add a new module
-- @function change_modules:add_module
function change_modules:add_module()
   -- prepare module options
   local available_modules = modpol.util.copy_table(modpol.modules)
   for k,org_mod in pairs(self.org.modules) do
      if available_modules[org_mod.slug] then
            available_modules[org_mod.slug] = nil
   end end
   -- present module options
   local modules_list = {}
   for k,v in pairs(available_modules) do
      table.insert(modules_list,v.name)
   end
   if #modules_list == 0 then
      modpol.interactions.message(
         self.initiator, "Org has all modules")
      modpol.interactions.org_dashboard(
         self.initiator, self.org.id)
      if self.data.result then self.data.result() end
      self.org:delete_process(self.id)
      return
   end
   table.sort(modules_list)
   -- now ask which to add
   modpol.interactions.dropdown_query(
      self.initiator, "Choose a module to add:",
      modules_list,
      function(mod_choice)
         -- confirm choice
         modpol.interactions.binary_poll_user(
            self.initiator,
            "Confirm: propose to add module \"" ..
            mod_choice .. "\"?",
            function(input)
               if input == "Yes" then
                  self:propose_change("add",mod_choice)
                  modpol.interactions.org_dashboard(
                     self.initiator, self.org.id)
               else
                  self:add_module()
               end
            end
         )
      end
   )
end

--- Remove a module
-- @function change_modules:remove_module
function change_modules:remove_module()
   -- prepare module options
   local available_modules = {}
   for k,org_mod in pairs(self.org.modules) do
      if not org_mod.hide then
         available_modules[org_mod.slug] = modpol.util.copy_table(org_mod)
   end end
   local modules_list = {}
   local modules_count = 0
   for k,v in pairs(available_modules) do
      table.insert(modules_list,v.name)
      modules_count = modules_count + 1
   end
   -- abort if no modules to remove
   if modules_count == 0 then
      modpol.interactions.message(
         self.initiator, "Org has no modules")
      modpol.interactions.org_dashboard(
         self.initiator, self.org.id)
      if self.data.result then self.data.result() end
      self.org:delete_process(self.id)
      return
   end
   table.sort(modules_list)
   -- now ask which to remove
   modpol.interactions.dropdown_query(
      self.initiator, "Choose a module to remove:",
      modules_list,
      function(mod_choice)
         -- confirm choice
         modpol.interactions.binary_poll_user(
            self.initiator,
            "Confirm: propose to remove module \"" .. mod_choice .. "\"?",
            function(input)
               if input == "Yes" then
                  self:propose_change("remove",mod_choice)
                  modpol.interactions.org_dashboard(
                     self.initiator, self.org.id)
               else
                  self:remove_module()
               end
            end
         )
      end
   )
end

--- Propose a change.
-- Type "add" or "remove"
-- @function change_modules:propose_change
-- @param type
-- @param mod_text
function change_modules:propose_change(type, mod_text)
   self:call_module(
      "consent",self.initiator,
      {
         prompt = "Do you consent to "..type..
            " this module in org "..self.org.name..
            ":\n"..mod_text,
         votes_required = #self.org.members
      },
      function()
         if type == "add" then
            for k,v in pairs(modpol.modules) do
               if v.name == mod_text then
                  table.insert(self.org.modules,v)
               end
            end
            modpol.interactions.message_org(
               self.initiator,self.org.id,
               "Consent reached:\nAdding \""
               ..mod_text.."\" to org "..self.org.name)
         elseif type == "remove" then
            local i = 0
            for k,v in pairs(self.org.modules) do
               i = i + 1
               if v.name == mod_text then
                  self.org.modules[k] = nil
               end                  
            end
            modpol.interactions.message_org(
               self.initiator,self.org.id,
               "Consent reached:\nRemoving \""
               ..mod_text.."\" from org "..self.org.name)
         end
   end)
   if self.data.result then self.data.result() end
   self.org:delete_process(self.id)
end
   
modpol.modules.change_modules = change_modules
