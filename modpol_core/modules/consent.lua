--- A utility module for checking consent
-- @module consent

local consent = {
   name = "Consent process",
   slug = "consent",
   desc = "A utility module other modules use for consent decisions",
   hide = true
}

consent.data = {
   votes = 0
}

consent.config = {
   prompt = "Do you consent?",
   votes_required = false
}

--- Initiate consent
-- @function consent:initiate
-- @param result Callback if this module is embedded in other modules
function consent:initiate(result)
   self.data.result = result
   -- if org is empty or no votes required, consent given
   if self.org:get_member_count() == 0
      or self.config.votes_required == 0 then
      modpol.interactions.message_org(
         self.initiator,
         self.org.name,
         "Consent reached: " .. self.config.prompt)
      if self.data.result then
         self.data.result() end
      self.org:delete_process(self.id)
   else
      -- otherwise, create poll
      -- set default votes_required
      if not self.config.votes_required then
         self.config.votes_required = self.org:get_member_count()
      end
      for id, member in pairs(self.org.members) do
         self.org:add_pending_action(self.id, member, "callback")
      end
   end
end

--- Callback
-- @function consent:callback
-- @param member
function consent:callback(member)
    modpol.interactions.binary_poll_user(
        member,
        self.config.prompt,
        function (resp)
           self.org:remove_pending_action(self.id,member)
           if resp == "Yes" then
              self.data.votes = self.data.votes + 1
           end
           modpol.interactions.message_org(
              "consent", self.org.id,
              member.." decided "..resp.." on: "..
              self.config.prompt.." ("..self.data.votes..
              "/"..self.config.votes_required..")"
           )
           if self.data.votes >= self.config.votes_required then
              modpol.interactions.message_org(
                 self.initiator,
                 self.org.name,
                 "Consent reached: " .. self.config.prompt)
              if self.data.result then
                 self.data.result() end
              self.org:delete_process(self.id)
           end
           modpol.interactions.org_dashboard(
              member, self.org.id)
        end
    )
end

modpol.modules.consent = consent
