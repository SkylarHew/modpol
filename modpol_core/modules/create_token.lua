--- Create token.
-- Depends on tokenomics
-- @module create_token

local create_token = {
    name = "Create a token",
    slug = "create_token",
    desc = "Creates an org token",
    hide = false;
}

create_token.data = {
}

create_token.config = {
   token_name = "token",
   approval_module = false
}

--- Initiate function
-- @function create_toke:initiate
-- @param result Callback if this module is embedded in other modules
function create_token:initiate(result)
   modpol.interactions.text_query(
      self.initiator,
      "Token name (alpha-numeric, no spaces):",
      function(input)
         self.config.token_name = input
         self:call_module(
            "tokenomics",
            self.initiator,
            {
               approval_module = self.config.approval_module,
               token_slug = self.config.token_name
            },
            function(input2)
               modpol.interactions.org_dashboard(
                  self.initiator, self.org.name)
               if result then result() end
               -- call this wherever process might end:
               self.org:delete_process(self.id)
            end
         )
         modpol.interactions.org_dashboard(
            self.initiator, self.org.name)
      end
   )
end

modpol.modules.create_token = create_token
