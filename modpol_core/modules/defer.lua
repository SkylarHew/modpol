--- Defer
-- @module defer

local defer = {
    name = "Defer ",
    slug = "defer",
    desc = "Defers a decision to another org",
    hide = true;
}

defer.data = {
}

--- Config for module 
-- @field defer_org Name or ID of target org
-- @field approval_module module to use in target org
-- @field prompt String passed on to approval_module
defer.config = {
   approval_module = "consent",
   defer_org = "Root",
   prompt = ""
}

--- Initiate function
-- @param result Callback if this module is embedded in other modules
-- @function defer:initiate
function defer:initiate(result)
   local defer_org = modpol.orgs.get_org(self.config.defer_org)
   if not defer_org then
      modpol.interactions.message(
         self.initiator, "Target org not found, aborting")
      self.org:delete_process(self.id)
   else
      defer_org:call_module(
         self.config.approval_module,
         self.initiator,
         {
            prompt = self.config.prompt
         },
         function()
            if result then result() end
      end)
      modpol.interactions.message(
         self.initiator, "Defer: action sent to " .. defer_org.name)
   end
   modpol.interactions.org_dashboard(
      self.initiator, self.org.id)
   self.org:delete_process(self.id)
end

--- (Required) Add to module table
modpol.modules.defer = defer
