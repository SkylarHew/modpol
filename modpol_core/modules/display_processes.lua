--- Display processes
-- @module display_processes

local display_processes = {
    name = "Display processes",
    slug = "display_processes",
    desc = "Presents a detailed list of org processes",
    hide = false;
}

display_processes.data = {
}

display_processes.config = {
}

--- Initiate function
-- @function display_processes:initiate
-- @param result Callback if this module is embedded in other modules
function display_processes:initiate(result)
   local display_table = {}
   for k,v in ipairs(self.org.processes) do
      if v ~= "deleted" then
         local input = v.id..": "..v.slug
         table.insert(display_table, input)
         input = "Org: "..v.org.name..
            ", initiator: "..v.initiator
         table.insert(display_table, input)
         if v.config
            and modpol.util.num_pairs(v.config) > 0 then
            table.insert(display_table, "Policies:")
            for k2,v2 in pairs(v.config) do
               local v2_string = ""
               if type(v2) == "string" then
                  v2_string = v2
               elseif type(v2) == "table"
                  or type(v2) == "number" then
                  v2_string = tostring(v2)
               else
                  v2_string = "Could not render"
               end
               input = k2..": "..v2_string
               table.insert(display_table, input)
            end
         end
         table.insert(display_table, "---")
      end
   end
   local output = table.concat(display_table,"\n")
   if #display_table == 0 then
      output = "No processes found"
   end
   modpol.interactions.display(
      self.initiator,
      "Processes in org "..self.org.name,
      output,
      function()
         modpol.interactions.org_dashboard(
            self.initiator, self.org.id)
         if result then result() end
         self.org:delete_process(self.id)
      end
   )
end

modpol.modules.display_processes = display_processes
