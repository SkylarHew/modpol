--- Removes initiator from org
-- @module leave_org

local leave_org = {
    name = "Leave org",
    slug = "leave_org",
    desc = "Remove yourself from the current org"
}

leave_org.data = {
}

leave_org.config = {
}

--- Removes user from org
-- @function leave_org:initiate
-- @param result Callback if this module is embedded in other modules
function leave_org:initiate(result)
   if self.org == modpol.instance then
      modpol.interactions.message(
         self.initiator,
         "You cannot leave the root org")
   else
      self.org:remove_member(self.initiator)
      modpol.interactions.message_org(
         self.initiator,self.org.id,
         self.initiator .. " has left org " .. self.org.name)
      modpol.interactions.message(
         self.initiator,
         "You have left org " .. self.org.name)
   end
   if result then result() end
   self.org:delete_process(self.id)
end

modpol.modules.leave_org = leave_org
