--- A utility module that outputs a random result from a set of options
-- @module randomizer

local randomizer = {
    name = "Randomizer",
    slug = "randomizer",
    desc = "A utility module other modules use for random decisions",
    hide = true
}

randomizer.data = {
}

-- options_table should be a table of strings
randomizer.config = {
    options_table = {},
    num_results = 1,
    result_table = {}
}

--- Initiate function
-- @function randomizer:initiate
-- @param result Callback if this module is embedded in other modules
function randomizer:initiate(result)
   self.data.result = result
   self.data.options_table = modpol.util.copy_table(self.config.options_table)
   -- if options table is empty, randomizer returns that
   if #self.data.options_table == 0 or self.config.num_results == 0 then
      if self.data.result then
         self.data.result({}) end
      self.org:delete_process(self.id)
   else
      -- otherwise, choose a random result
      self.random_loop()
   end
end

--- Returns result_table
-- @function randomizer:random_loop
function randomizer:random_loop()
   self.data.results = 0
   if results == self.config.num_results then
      self.data.result(self.data.result_table)
   else
      math.randomseed(os.time())
      local index = math.random(self.data.options_table)
      table.insert(self.data.result_table, self.data.options_table[index])
      table.remove(self.data.options_table, index)
      self.data.results = self.data.results + 1
   end
end
   
modpol.modules.randomizer = randomizer
