--- Remove child org
-- A simple module that calls a process on an org to remove its child.
-- @module remove_child_org

local remove_child_org = {
    name = "Remove child org",
    slug = "remove_child_org",
    desc = "Removes a child org."
}

remove_child_org.data = {
   result = nil,
   child_to_remove = nil
}

remove_child_org.config = {
   approval_module = false
}

--- Removes a child org with consent
-- @function remove_child_org:initiate
-- @param result Callback if this module is embedded in other modules
function remove_child_org:initiate(result)
   local children = {}
   for i,v in ipairs(self.org.children) do
      local child = modpol.orgs.get_org(v)
      if child then table.insert(children, child.name) end
   end
   -- Abort if no child orgs
   if #children == 0 then
      modpol.interactions.message(
         self.initiator,
         "Org has no children")
      if result then result() end
      self.org:delete_process(self.id)
   else
      self.data.result = result
      modpol.interactions.dropdown_query(
         self.initiator,
         "Choose a child of org "..
         self.org.name.." to remove:",
         children,
         function(input)
            self.data.child_to_remove = modpol.orgs.get_org(input)
            self:call_module(
               self.config.approval_module,
               self.initiator,
               {
                  prompt = "Remove child org "..input.."?"
               },
               function()
                  self:complete()
            end)
            modpol.interactions.org_dashboard(
               self.initiator, self.org.name)
      end)
   end
end

--- Complete the remove process
-- @function remove_child_org:complete
function remove_child_org:complete()
   modpol.interactions.message_org(
      self.initiator, self.data.child_to_remove.id,
      "Removing this org: " .. self.data.child_to_remove.name)
   modpol.interactions.message_org(
      self.initiator, self.org.id,
      "Removing child org of " .. self.org.name .. ": " ..
      self.data.child_to_remove.name)
   self.data.child_to_remove:delete()
   if self.data.result then self.data.result() end
   self.org:delete_process(self.id)
end

modpol.modules.remove_child_org = remove_child_org
