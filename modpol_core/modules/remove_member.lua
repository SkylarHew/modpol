--- Removes member from org
-- @module remove_member

local remove_member = {
    name = "Remove a member",
    slug = "remove_member",
    desc = "Removes org member"
}

remove_member.data = {
   member_to_remove = "",
   result = nil
}

remove_member.config = {
   approval_module = false
}

--- Removes given member from org
-- @function remove_member:initiate
-- @param result Callback if this module is embedded in other modules
function remove_member:initiate(result)
   -- Abort if in root org
   if self.org == modpol.instance then
      modpol.interactions.message(
         self.initiator,
         "Members cannot be removed from the root org")
      modpol.interactions.org_dashboard(
         self.initiator, self.org.name)
      if result then result() end
      self.org:delete_process(self.id)
   else -- proceed if not root
      self.data.result = result
      modpol.interactions.dropdown_query(
         self.initiator,
         "Which member of org "..self.org.name..
         " do you want to remove?",
         self.org.members,
         function(input)
            self.data.member_to_remove = input
            self:call_module(
               self.config.approval_module,
               self.initiator,
               {
                  prompt = "Remove "..input..
                     " from org "..self.org.name.."?"
               },
               function()
                  self:complete()
            end)
            modpol.interactions.org_dashboard(
         self.initiator, self.org.name)
      end)
   end
end

--- Complete after consent
-- @function remove_member:complete
function remove_member:complete()
   modpol.interactions.message_org(
      self.initiator, self.org.id,
      "Removing "..
      self.data.member_to_remove..
      " from org "..self.org.name)
   self.org:remove_member(self.data.member_to_remove)
   self.org:delete_process(self.id)
   if self.data.result then self.data.result() end
end

modpol.modules.remove_member = remove_member
