--- Remove org
-- Removes the current org
-- @module remove_org

local remove_org = {
    name = "Remove this org",
    slug = "remove_org",
    desc = "Removes this org."
}

remove_org.data = {
   result = nil
}

remove_org.config = {
   approval_module = false
}

--- Remove org if all members consent
-- @function remove_org:initiate
-- @param result Callback if this module is embedded in other modules
function remove_org:initiate(result)
   if self.org == modpol.instance then
      modpol.interactions.message(
         self.initiator,
         "Cannot remove root org")
      if result then result() end
      self.org:delete_process(self.id)
   else
      self.data.result = result
      self:call_module(
        self.config.approval_module, 
        self.initiator, 
        {
           prompt = "Remove org " .. self.org.name .. "?"
        },
        function()
           self:complete()
        end
      )
      modpol.interactions.org_dashboard(
         self.initiator, modpol.orgs.get_org(self.org.parent).name)
   end
end

--- Complete after approval
-- @function remove_org:complete
function remove_org:complete()
   modpol.interactions.message_org(
      self.initiator, self.org.id,
      "Removing org " .. self.org.name)
   if self.data.result then self.data.result() end
   self.org:delete_process(self.id)
   self.org:delete()
end

modpol.modules.remove_org = remove_org
