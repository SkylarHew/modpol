--- Remove a process
-- @module remove_process

local remove_process = {
    name = "Remove process",
    slug = "remove_process",
    desc = "User can remove own processes, approval required for those of others",
    hide = false;
}

remove_process.data = {
}

remove_process.config = {
   approval_module = "consent"
}

--- Initiate function
-- @function remove_process:initiate
-- @param result Callback if this module is embedded in other modules
function remove_process:initiate(result)
   -- prepare process options
   local available_processes = {}
   for k,process in pairs(self.org.processes) do
      if process ~= "deleted" then
         available_processes[process.id] = process
      end
   end
   local process_list = {}
   local process_count = 0
   for k,v in pairs(available_processes) do
      local mine = ""
      if v.initiator == self.initiator then mine = "*" end
      table.insert(process_list,"["..v.id.."] "..v.slug..mine)
      process_count = process_count + 1
   end
   -- abort if no processes to remove
   if process_count == 0 then
      modpol.interactions.message(
         self.initiator, "Org has no modules")
      modpol.interactions.org_dashboard(
         self.initiator, self.org.id)
      if result then result() end
      self.org:delete_process(self.id)
      return
   end
   table.sort(process_list)
   -- now ask which to remove
   modpol.interactions.dropdown_query(
      self.initiator, "Choose a process to remove (* marks yours, no consent required):",
      process_list,
      function(process_choice)
         -- confirm choice
         local process_id = tonumber(
            string.match(process_choice, "%d+"))
         local process_mine = string.match(process_choice,
                                           "%*")
         modpol.interactions.binary_poll_user(
            self.initiator,
            "Confirm: Remove process \""..
            process_choice .. "\"?",
            function(input)
               if input == "Yes" then
                  if process_mine then
                     self.org:delete_process_tree(process_id)
                     modpol.interactions.message(
                        self.initiator,
                        "Removed process: "..process_choice)
                     modpol.interactions.org_dashboard(
                        self.initiator, self.org.id)
                     if result then result() end
                     self.org:delete_process(self.id)
                  else
                     self:call_module(
                        self.config.approval_module,
                        self.initiator,
                        {
                           prompt =
                              "Approve removal of process "
                              ..process_choice.."?"
                        },
                        function(input)
                           modpol.interactions.message_org(
                              self.initiator,
                              self.org.id,
                              "Removing process: "..
                              process_choice)
                           self.org:delete_process_tree(process_id)
                           modpol.interactions.org_dashboard(
                              self.initiator, self.org.id)
                           if result then result() end
                           self.org:delete_process(self.id)
                        end
                     )
                  end
                  modpol.interactions.org_dashboard(
                     self.initiator, self.org.id)
               else
                  modpol.interactions.org_dashboard(
                     self.initiator, self.org.id)
                  if result then result() end
                  self.org:delete_process(self.id)
               end
            end
         )
      end
   )
end

modpol.modules.remove_process = remove_process
