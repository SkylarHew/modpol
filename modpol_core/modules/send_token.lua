--- Send token.
-- Depends on tokenomics
-- @module send_token

local send_token = {
    name = "Send tokens",
    slug = "send_token",
    desc = "Send tokens to another user",
    hide = false;
}

--- (Required) Data for module
-- Variables that module uses during the course of a process
-- Can be blank
send_token.data = {
}

send_token.config = {
   token_name = nil -- hidden configuration
}

--- initiate function
-- @function send_token:initiate
-- @param result Callback if this module is embedded in other modules
function send_token:initiate(result)
   local token_list = {}
   if self.org.tokens then
      for k,v in pairs(self.org.tokens) do
         table.insert(token_list, k)
      end
   end
   if #token_list == 0 then
      modpol.interactions.message(
         self.initiator,
         "No tokens in org")
      modpol.interactions.org_dashboard(
         self.initiator, self.org.name)
      self.org:delete_process(self.id)
      return
   else
      modpol.interactions.dropdown_query(
         self.initiator,
         "Which token do you want to send?",
         token_list,
         function(input_token)
            modpol.interactions.dropdown_query(
               self.initiator,
               "Who do you want to send to?",
               modpol.util.copy_table(self.org.members),
               function(input_recipient)
                  modpol.interactions.text_query(
                     self.initiator,
                     "How much do you want to give (a number)?",
                     function(input_amount)
                        modpol.modules.tokenomics.transfer(
                           self.org.id,
                           input_token,
                           self.initiator,
                           input_recipient,
                           input_amount
                        )
                        modpol.interactions.org_dashboard(
                           self.initiator, self.org.name)
                        -- close process
                        if result then result() end
                        self.org:delete_process(self.id)
                     end
                  )
               end
            )
         end
      )
   end
end

modpol.modules.send_token = send_token
