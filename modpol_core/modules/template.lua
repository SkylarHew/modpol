--- Template for module writers.
-- Short description goes here.
-- @module module_template

--- (Required): Data table containing name and description of the module
-- @field name "Human-readable name (parens OK, no brackets)"
-- @field slug "Same as module class name"
-- @field desc "Description of the module"
-- @field hide Whether this is a hidden utility module
local module_template = {
    name = "Module Human-Readable Name",
    slug = "module_template",
    desc = "Description of the module",
    hide = false;
}

--- (Required) Data for module
-- Variables that module uses during the course of a process.
-- Can be blank
module_template.data = {
}

--- (Required): Config for module 
-- Defines the input parameters to the module initiate function.
-- Can be blank
-- When calling a module from within another module,
-- variables not defined in config will be ignored.
-- Default values set in config can be overridden
-- @field approval_module names a module that must pass before approval; defaults to false
-- @field field_1 ex: votes_required, default = 5
-- @field field_2 ex: voting_type, default = "majority"
module_template.config = {
    approval_module = false -- visible but empty
    hidden_config = nil -- not visible to users unless used
    field_1 = 5
    field_2 = "majority"
}

--- (Required): Initiate function
-- Modules have access to the following instance variables:
-- <li><code>self.org</code> (the org the module was called in),</li>
-- <li><code>self.initiator</code> (the user that callced the module),</li>
-- <li><code>self.id</code> (the process id of the module instance)</li>
-- @function module_template:initiate
-- @param result (optional) Callback if this module is embedded in other modules
function module_template:initiate(result)
   -- call interaction functions here!

   -- concluding functions:
   -- first, where appropriate, return users to dashboards.
   -- second, result:
   -- may need to put result in self.data.result
   -- call this when module is successful (not for abort):
   if result then result() end
   -- third, delete the process
   -- call this wherever process might end:
   self.org:delete_process(self.id)
end

--- (Required) Add to module table
modpol.modules.module_template = module_template
