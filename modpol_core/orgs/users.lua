-- /users.lua
-- User-related functions for Modular Politics

-- ===================================================================
-- Function: modpol.list_users
-- Params: org
-- Outputs: Table of user names
--
-- This may be overwritten by the platform-specific interface

modpol.list_users = function(org)
   local users = {}
   if (org == nil) then -- no specified org; all players
      if modpol.orgs["instance"]
         and modpol.orgs["instance"]["members"] then
         -- if instance exists and has membership
         users = modpol.orgs["instance"]["members"]
      else
         users = {}
      end
   else -- if an org is specified
      if (modpol.orgs[org] ~= nil) then -- org exists
         users = modpol.orgs[org]["members"]
      end
   end
   return users
end
