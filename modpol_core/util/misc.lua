--- Miscellaneous functions
-- @module modpol.util.misc

modpol.util = {}

--- Returns a copy of the table inputted
-- @function modpol.util.copy_table
-- @param t table to copy
-- @return copy of table
function modpol.util.copy_table(t)
   local t2 = {}
   if pairs(t) then
      for k,v in pairs(t) do
         if type(v) == "table" then
            t2[k] = modpol.util.copy_table(v)
         else
            t2[k] = v
         end
      end
   end
   return t2
end

--- Returns the number of elements in a pairs table
-- @function modpol.util.num_pairs
-- @param t pairs table
-- @return number of elements in pairs table
function modpol.util.num_pairs(t)
   local i = 0
   for k,v in pairs(t) do
      i = i + 1
   end
   return i
end

function modpol.util.lazy_table_length(tbl, lazy_val)
   local count = 0
   for k, v in ipairs(tbl) do
      if v ~= lazy_val then
         count = count + 1
      end
   end
   return count
end
