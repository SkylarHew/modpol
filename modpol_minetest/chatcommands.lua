-- ===================================================================
-- Minetest commands
-- ===================================================================

command_list = {}               -- user-facing table of commands

local chat_table                -- MT chat command definitions table
local regchat                   -- Chat-command registration function

regchat = minetest.register_chatcommand

regchat = function(name, command_table)
   minetest.register_chatcommand(name, command_table)
   table.insert(command_list, name)
end

-- ===================================================================
-- /mp
-- Presents a menu of options to users
regchat(
   "mp", {
      privs = {},
      func = function(user)
         modpol.interactions.dashboard(user)
      end,
})

-- ===================================================================
-- /mptest
-- For testing only, accessible to admin users
-- Clears the system and recreates instance with all players
-- opens dashboard too for fun.
regchat(
   "mptest", {
      privs = {privs=true},
      func = function(user)
         modpol.instance.members = modpol.list_users()
         modpol.orgs.reset()
         modpol.interactions.dashboard(user)
         return true, "Reset orgs"
      end,
})



-- ===================================================================
-- /addorg
-- This code defines a chat command which creates a new
-- "org". Presently, the command makes the user the sole member of the
-- "org".

regchat(
   "addorg", {
      privs        = {} ,
      func         = function (user, param)
         local success, message = modpol.instance:add_org (param)
         return true, message
      end        
})

-- ===================================================================
-- /listorgs
-- In Minetest mode, this code defines a chat command which lists
-- existing "orgs".
-- The list shows one "org" per line in the following format:
-- org_name (member, member, ...)

regchat(
   "listorgs", {
      privs = {} ,
      func  = function (user, param)
         return true, "Orgs: " ..
            table.concat(modpol.orgs.list_all(), ", ")
      end
})

